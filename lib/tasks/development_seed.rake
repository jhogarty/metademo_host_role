unless ENV['RAILS_ENV'] == 'production' 
  require 'fabrication'

  namespace :dev do
    desc "Seed development database, maintain consistency"
    task prime: :environment do

      HostRole.delete_all

      Fabricate(:host_role)
      Fabricate(:app_server)
      Fabricate(:web_server)
      Fabricate(:db_server)
      Fabricate(:container_host)

      puts "Created #{HostRole.count} records for the Host Role table."
    end
  end

end

