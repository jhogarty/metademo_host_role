class CreateHostRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :host_roles do |t|
      t.string :name
      t.string :description
      t.string :created_by
      t.datetime :created_dttm
      t.string :updated_by
      t.datetime :updated_dttm
    end
    add_index :host_roles, :name, unique: true
  end
end
