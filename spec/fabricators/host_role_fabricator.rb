Fabricator(:host_role) do
  name        "generic"
  description "Generic host role"
  created_by  "MetaDemo::HostRole"
  created_dttm  Time.now
  updated_by  nil
  updated_dttm nil
end

Fabricator(:app_server, from: :host_role) do
  name        "App Server"
  description "Use for hosts that are application servers"
end

Fabricator(:web_server, from: :host_role) do
  name        "Web Server"
  description "Use for hosts that are web servers"
end

Fabricator(:db_server, from: :host_role) do
  name        "Database Server"
  description "Use for hosts that are database servers"
end

Fabricator(:container_host, from: :host_role) do
  name        "Docker Host"
  description "Use for hosts that will run docker containers"
end
